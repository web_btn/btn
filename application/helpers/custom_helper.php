<?php
function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}
function bcrypt($string)
{
    return password_hash($string, PASSWORD_BCRYPT);
}

function verifyPassword($db_password, $password_check)
{
    return password_verify($password_check, $db_password);
}
function saveInput($string){
    if(!is_array($string)){
        return htmlentities($string,ENT_QUOTES, 'UTF-8');
    } else {
        return $string;
    }

}

function getNode($node){
    $ex = explode("SpacesStore/", $node);
    return $ex[1];
}


