<!DOCTYPE html>
<html>
<head>
	<title>test</title>
	<base href="<?=base_url()?>">
	<link href="assets/sb/css/sb-admin-2.css" rel="stylesheet">
</head>
<body>
	<div id="list-folder">
	<?php
	echo "<ul>";
	foreach ($data['items'] as $key) {
		$url = $key['webdavUrl'];
		$url = str_replace("/webdav", "", "$url");
       	$id = AltUuid::generateRandomString();
		echo "<li class='list-item' id='$id'><a href class='list' data-path='$url' data-id='$id'>";
		echo $key['node']['properties']['cm:name'];

		echo "</a></li>";
	}
	echo "</ul>";

	?>
</div>


	<script src="assets/sb/plugins/jquery/dist/jquery.min.js"></script>
	<script type="text/javascript">
		$(function(){
			
			$('#list-folder').on('click','.list',function(e){
				e.preventDefault();
				var path = $(this).data('path');
				var id = $(this).data('id');
				var base_url = $('base').attr('href');
				$.ajax({
					url: base_url+"context/mine/test2",
					data: 'path='+path,
					dataType: "html",					
					type: "POST",
					success: function(data) {
						console.log(id);
						var x = $('#'+id).html();
						$('#'+id).html(x+data);
						$('.list').on('click',function(e){
				e.preventDefault();
			});
					},
					error: function(xhr, status) {
						console.log(xhr);
					}
				});
			});
		});
	</script>
	<script src="assets/sb/js/sb-admin-2.js"></script>
</body>
</html>