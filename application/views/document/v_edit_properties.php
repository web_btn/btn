
<form class="form-horizontal form-ajax" id="form-input" action="document/operation/edit_properties">
  <input type="hidden" name="node" class="node">
  <div class="modal-body">
    
     
  <div class="form-group">
    <label class="col-sm-2 control-label">Name <font color="green">*</font></label>
    <div class="col-sm-10">
      <input type="text" value="<?=$detail['item']['node']['properties']['cm:name']?>" class="form-control" name="name" required="">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-2 control-label">Title</label>
    <div class="col-sm-10">
      <input type="text" value="<?=isset($detail['item']['node']['properties']['cm:title'])? $detail['item']['node']['properties']['cm:title'] : null;?>" class="form-control" name="title">
    </div>
  </div>
  
  <div class="form-group">
    <label class="col-sm-2 control-label">Description</label>
    <div class="col-sm-10">
      <textarea class="form-control" name="description"><?=isset($detail['item']['node']['properties']['cm:description']) ? $detail['item']['node']['properties']['cm:description'] : null;?></textarea>
    </div>
  </div>
 </div>
  <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
</form>