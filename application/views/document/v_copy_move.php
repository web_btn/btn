
<form class="form-horizontal form-ajax" action="" enctype="multipart/form-data" method="post" id="form-input">
	<style type="text/css">
		.active-path{
			display: block;
		    padding-left: 10px;
		    list-style: none;
		    background-color: #f1f1f1;
		}
		ul, .list-destination{
			list-style-type: none;
		}
	</style>
	<div class="modal-body"> 
		<!-- <input type="text" id="path"> -->
		<div class="row">
			<div class="col-md-4">
				<ul class="nav nav-pills nav-stacked">
					<li class="active"><a data-toggle="tab" href="" id="destination-repository">Repository</a></li>
					<li><a data-toggle="tab" href="" id="destination-shared">Shared Files</a></li>
					<li><a data-toggle="tab" href="" id="destination-myfiles">My Files</a></li>
				</ul>
			</div>
			<div class="col-md-8">
				<div id="list-folder-item">
					<div id="destination">
						<div id="list-folder">
							
						</div>
					</div>
					
				</div>
			</div>
		</div>
		<br>
		<div class="list-group nodeRefs">

		</div>

	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		<button type="submit" class="btn btn-primary">Save</button>
	</div>
</form>