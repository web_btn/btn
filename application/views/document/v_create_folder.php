
<form class="form-horizontal form-ajax" id="form-input" action="document/operation/create_folder">
  <input type="hidden" name="node" class="node">
  <div class="modal-body">
    <h3>New Folder Details</h3>    
     <span><font color="green">*</font> Required Fields</span>
      <br>
      <br>
      
  <div class="form-group">
    <label class="col-sm-2 control-label">Name <font color="green">*</font></label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="name" required="">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-2 control-label">Title</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="title">
    </div>
  </div>
  
  <div class="form-group">
    <label class="col-sm-2 control-label">Description</label>
    <div class="col-sm-10">
      <textarea class="form-control" name="description"></textarea>
    </div>
  </div>
 </div>
  <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
</form>