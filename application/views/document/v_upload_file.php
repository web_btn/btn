
<form class="form-horizontal form-ajax" action="<?=$this->API."/api/upload?alf_ticket=".$this->ticket."";?>" enctype="multipart/form-data" method="post" id="form-input">
  <?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: *");
?>
  <input type="hidden" name="node" class="node">
  <input type="hidden" name="destination" class="destination">
  <input type="hidden" name="overwrite" value="false">
  <div class="modal-body">
    
      
  <div class="form-group">
    <label class="col-sm-2 control-label">File <font color="green">*</font></label>
    <div class="col-sm-10">
      <input type="file" class="form-control" name="filedata" required="">
    </div>
  </div>
  
 </div>
  <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
</form>