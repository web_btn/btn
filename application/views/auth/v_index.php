<!DOCTYPE html>
<html>
<head>
    <title>BNPT || Badan Nasional Penanggulangan Terorisme </title>
    <base href="<?=base_url()?>">
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link type="png/img" rel="icon" href="assets/img/LOGO-BNPT.png"  media="screen,projection"/>
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="assets/dist/css/materialize.min.css"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="assets/dist/css/animate.css"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="assets/dist/css/page-login.css"  media="screen,projection"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>
<body>

	<div class="row">
		<div class="col s12 m4 l4 "></div>
		<div class="col s12 m4 l4 top20 animated slideInDown">
			<div class="card">
				<div class="card-heading">
					<div class="logo">
						<img src="assets/img/logo_login.png" width="70%">
					</div>
				</div>
				<div class="card-content">
					<div class="row">
						<form action="auth/login" method="POST">
						<div class="input-field col s12">
							<i class="material-icons prefix">account_circle</i>
							<input id="username" type="text" name="username" class="validate">
	          				<label for="username">ID Pengguna</label>
						</div>
						<div class="input-field col s12">
							<i class="material-icons prefix">lock</i>
							<input id="password" type="password" name="password" class="validate">
	          				<label for="password">Kata Sandi</label>
							
						</div>
						 <button class="btn orange darken-1  waves-effect waves-light right" type="submit">Masuk</button>
						
						</form>
					</div>
				</div>
			</div>
			<div class="form-footer">
				CopyRight &copy 2017 All Right Reserved
			</div>
		</div>
	</div>

	<!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="assets/dist/js/jquery.js"></script>
    <script type="text/javascript" src="assets/dist/js/materialize.min.js"></script>
    <script language="JavaScript">
		var txt="BNPT || Badan Nasional Penanggulangan Terorisme ";
		var kecepatan=150;
		var segarkan=null;
		function bergerak() 
			{ document.title=txt;
				txt=txt.substring(1,txt.length)+txt.charAt(0);
				segarkan=setTimeout("bergerak()",kecepatan);
			}
			bergerak();
	</script>
</body>
</html>