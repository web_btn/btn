<div class="form-ajax">
	<div class="card-content">
		<form action="<?=base_url("document/workflow/generate")?>" method="POST" class="form-horizontal">
			<input type="hidden" name="file" value="Sprint Ka. BNPT1">
		  	<div class="form-group">
			    <label class="col-sm-2 control-label">Pertimbangan</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" name="pertimbangan">
			    </div>
		  	</div>
		  	<div class="form-group">
			    <label class="col-sm-2 control-label">Kepada</label>
			    <div class="col-sm-5">
			      <input type="text" class="form-control" name="nama1" placeholder="Nama">
			    </div>
			    <div class="col-sm-5">
			      <input type="text" class="form-control" name="jabatan1" placeholder="Jabatan">
			    </div>
		  	</div>
		  	<div class="form-group">
			    <label class="col-sm-2 control-label"></label>
			    <div class="col-sm-5">
			      <input type="text" class="form-control" name="nama2" placeholder="Nama">
			    </div>
			    <div class="col-sm-5">
			      <input type="text" class="form-control" name="jabatan2" placeholder="Jabatan">
			    </div>
		  	</div>
		  	<div class="form-group">
			    <label class="col-sm-2 control-label">Untuk</label>
			    <div class="col-sm-2 w18">
			      <input type="text" class="form-control" name="untuk_1" placeholder="1">
			    </div>
			    <div class="col-sm-2 w18">
			      <input type="text" class="form-control" name="untuk_2" placeholder="2">
			    </div>
			    <div class="col-sm-2 w18">
			      <input type="text" class="form-control" name="untuk_3" placeholder="3">
			    </div>
			    <div class="col-sm-2 w18">
			      <input type="text" class="form-control" name="untuk_4" placeholder="4">
			    </div>
		  	</div>
		  	<div class="form-group">
			    <label class="col-sm-2 control-label">Dikeluarkan di</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" name="dikeluarkan">
			    </div>
		  	</div>
		  	<div class="form-group">
			    <label class="col-sm-2 control-label">Pada Tanggal</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" name="pada_tanggal">
			    </div>
		  	</div>
		  	<div class="form-group">
	    		<div class="col-sm-offset-2 col-sm-10">
	      			<button type="submit" class="btn btn-default" name="generate">Generate</button>
	    		</div>
	  		</div>
		</form>
	</div>
</div>