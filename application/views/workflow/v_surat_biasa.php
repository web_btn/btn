<div class="form-ajax">
	<div class="card-content">
		<form action="<?=base_url("document/workflow/generate")?>" method="POST" class="form-horizontal">
			<input type="hidden" name="file" value="Surat Biasa Ka. BNPT1">
			<div class="form-group">
				<label class="col-sm-2 control-label">Nomor</label>
				<div class="col-sm-10">
					<input type="number" class="form-control" name="nomor">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Sifat</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="sifat">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Lampiran</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="lampiran">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Perihal</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="perihal">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Kepada</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="kepada">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Tempat</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="tempat">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2">Nama</label>
				<div class="col-sm-2 w18">
					<input type="text" class="form-control" name="nama_1" placeholder="1">
				</div>
				<div class="col-sm-2 w18">
					<input type="text" class="form-control" name="nama_2" placeholder="2">
				</div>
				<div class="col-sm-2 w18">
					<input type="text" class="form-control" name="nama_3" placeholder="3">
				</div>
				<div class="col-sm-2 w18">
					<input type="text" class="form-control" name="nama_4" placeholder="4">
				</div>
			</div>	
			<div class="form-group">
				<label class="control-label col-sm-2">Jabatan</label>
				<div class="col-sm-2 w18">
					<input type="text" class="form-control" name="jabatan_1" placeholder="1">
				</div>
				<div class="col-sm-2 w18">
					<input type="text" class="form-control" name="jabatan_2" placeholder="2">
				</div>
				<div class="col-sm-2 w18">
					<input type="text" class="form-control" name="jabatan_3" placeholder="3">
				</div>
				<div class="col-sm-2 w18">
					<input type="text" class="form-control" name="jabatan_4" placeholder="4">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2">Pangkat</label>
				<div class="col-sm-2 w18">
					<input type="text" class="form-control" name="pangkat_1" placeholder="1">
				</div>
				<div class="col-sm-2 w18">
					<input type="text" class="form-control" name="pangkat_2" placeholder="2">
				</div>
				<div class="col-sm-2 w18">
					<input type="text" class="form-control" name="pangkat_3" placeholder="3">
				</div>
				<div class="col-sm-2 w18">
					<input type="text" class="form-control" name="pangkat_4" placeholder="4">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-default" name="generate">Generate</button>
				</div>
			</div>
		</form>
	</div>
</div>