<div class="form-ajax">
	<div class="card-content">
		<form action="<?=base_url("document/workflow/generate")?>" method="POST" class="form-horizontal">
			<input type="hidden" name="file" value="SPTJM DE II1">
		  	<div class="form-group">
			    <label class="col-sm-2 control-label">Nama</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" name="nama">
			    </div>
		  	</div>
		  	<div class="form-group">
			    <label class="col-sm-2 control-label">Jabatan</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" name="jabatan">
			    </div>
		  	</div>
		  	<div class="form-group">
			    <label class="col-sm-2 control-label">Isi</label>
			    <div class="col-sm-10">
			      <textarea class="form-control" name="isi_1" placeholder="1" rows="5"></textarea>
			    </div>
			</div>
		  	<div class="form-group">
			    <div class="col-sm-10 col-md-offset-2">
			      <textarea class="form-control" name="isi_2" placeholder="2" rows="5"></textarea>
			    </div>
		  	</div>
			<div class="form-group">
			    <label class="col-sm-2 control-label">Penutup</label>
			    <div class="col-sm-10">
			      <textarea class="form-control" name="penutup" rows="5"></textarea>
			    </div>
		  	</div>
		  	<div class="form-group">
	    		<div class="col-sm-offset-2 col-sm-10">
	      			<button type="submit" class="btn btn-default" name="generate">Generate</button>
	    		</div>
	  		</div>
		</form>
	</div>
</div>