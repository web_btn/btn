<div class="form-ajax">
	<div class="card-content">
		<form action="<?=base_url("document/workflow/generate")?>" method="POST" class="form-horizontal">
			<input type="hidden" name="file" value="Nota Dinas DE II">
		  	<div class="form-group">
			    <label class="col-sm-2 control-label">Kepada</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" name="kepada">
			    </div>
		  	</div>
		  	<div class="form-group">
			    <label class="col-sm-2 control-label">Dari</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" name="dari">
			    </div>
		  	</div>
			<div class="form-group">
			    <label class="col-sm-2 control-label">Perihal</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" name="perihal">
			    </div>
		  	</div>
		  	<div class="form-group">
			    <label class="col-sm-2 control-label">Tanggal</label>
			    <div class="col-sm-10">
			      <input type="date" class="form-control" name="tanggal">
			    </div>
		  	</div>
		  	<div class="form-group">
	    		<div class="col-sm-offset-2 col-sm-10">
	      			<button type="submit" class="btn btn-default" name="generate">Generate</button>
	    		</div>
	  		</div>
		</form>
	</div>
</div>