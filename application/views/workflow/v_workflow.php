 <?php
 $this->load->view('layout/layout_sidebar');
 ?>
 <div class="media" style="margin-top: 0px; padding: 10px;">
 	<div class="media-left media-middle">
 		<a href="">
 			<img class="media-object" src="assets/img/LOGO-BNPT.png" width="50px">
 		</a>
 	</div>
 	<div class="media-body" style="vertical-align: middle;">
 		<h3 class="media-heading" style="font-family: 'Lato Light';">Start Workflow</h3>
 	</div>
 </div>



 <div id="page-wrapper">
 	<div class="row">
 		<div class="col-lg-12">
 			<div class="card">
	 			<div class="card-head">
		 			<div class="row">
		 				<div class="col-md-4">
		 					<div class="form-group">
		 						<label>Workflow</label>
		 						<select class="form-control" id="select-workflow">
		 							<option hidden>Please Select a workflow</option>
		 							<option data-view="memo">Memo</option>
		 							<option data-view="nota">Nota Dinas</option>
		 							<option data-view="sprint">Sprint</option>
		 							<option data-view="sptjm">SPTJM</option>
		 							<option data-view="surat_biasa">Surat Biasa</option>
		 							<option data-view="surat_rahasia">Surat Rahasia</option>
		 						</select>
		 					</div>
		 				</div>
		 			</div>
	 			</div>
 				<div class="row">
	 				<div class="col-md-12" id="workflow">
	 					
	 				</div>
	 			</div>
	 		</div>
 		</div>
 	</div>
 </div>
        <!-- /page-wrapper -->