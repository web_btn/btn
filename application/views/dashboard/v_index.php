<!DOCTYPE html>
<html>
<head>
  <title>BNPT || Badan Nasional Penanggulangan Terorisme </title>
  <base href="<?=base_url();?>">
  <!--Import Google Icon Font-->
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <link type="png/img" rel="icon" href="assets/img/LOGO-BNPT.png"  media="screen,projection"/>
  <link type="text/css" rel="stylesheet" href="assets/dist/css/materialize.min.css"  media="screen,projection"/>
  <link type="text/css" rel="stylesheet" href="assets/dist/css/animate.css"  media="screen,projection"/>
  <link type="text/css" rel="stylesheet" href="assets/dist/css/BNPT.css"  media="screen,projection"/>

  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>

  <div class="header text-center">
    <div class="row">
      <div class="col s12 m6 l2">
        <div class="logo-brand">
          <a href="#!"><img src="assets/img/LOGO-BNPT.png"></a>
        </div>
      </div>
      <div class="col s12 m6 l8">
        <div class="title-header">
          <span class="white-text">Badan Nasional Penanggulangan Terorisme<h1 style="font-family: 'Lato Black';">BNPT</h1></span>
        </div>
      </div>
      <div class="col s12 m6 l2">
        <div class="menu-header">
          <a href="#!" class="btn btn-large yellow darken-2 waves-effect waves-light">Menu</a>
        </div>
      </div>
    </div>
  </div>


  <div class="row">
    <div class="col s12 m6 l1"></div>
    <div class="col s6 m6 l2 animated zoomIn">
      <div class="card">
        <div class="card-image"><a href="pages/panels-wells.html">
          
          <img width="100%" src="assets/img/deputi_2.png" />
        </a></div>
        
        <div class="card-action">
          <a href="context/mine/myfiles"><b>Deputi II</b></a>
          <br>
          <i>BNPT</i>
        </div>
      </div>
    </div>
    <div class="col s6 m6 l2 animated zoomIn">
      <div class="card">
        <div class="card-image"><a href="pages/panels-wells.html"><img width="100%" src="assets/img/tu_de_2.png"/></a></div>
        <div class="card-action">
          <a href="company""><b>TU DE II</b></a>
          <br>
          <i>BNPT</i>
        </div>
      </div>
    </div>
    <div class="col s6 m6 l2 animated zoomIn">
      <div class="card">
        <div class="card-image"><a href="pages/panels-wells.html"><img width="100%" src="assets/img/pembinaan.png"/></a></div>
        <div class="card-action">
          <a href="company""><b>Direktorat Pembinaan Kemampuan</b></a>
          <br>
          <i>BNPT</i>
        </div>
      </div>
    </div>
    <div class="col s6 m6 l2 animated zoomIn">
      <div class="card">
        <div class="card-image"><a href="pages/panels-wells.html"><img width="100%" src="assets/img/penegakan.png"/></a></div>
        <div class="card-action">
          <a href="company""><b>Direktorat Penegakan Hukum</b></a>
          <br>
          <i>BNPT</i>
        </div>
      </div>
    </div>
    <div class="col s6 m6 l2 animated zoomIn">
      <div class="card">
        <div class="card-image"><a href="pages/panels-wells.html"><img width="100%" src="assets/img/penindakan.png"/></a></div>
        <div class="card-action">
          <a href="company""><b>Direktorat Penindakan</b></a>
          <br>
          <i>BNPT</i>
        </div>
      </div>
    </div>
  </div>

  <div class="footer">
    <div class="footer-copyright white text-center">
      <a class="grey-text" href="">Copyright &copy BNPT All Right reserved</a>
    </div>
  </div>

  <!--Import jQuery before materialize.js-->
  <script type="text/javascript" src="assets/dist/js/jquery.js"></script>
  <script type="text/javascript" src="assets/dist/js/materialize.min.js"></script>
  <script language="JavaScript">
    var txt="BNPT || Badan Nasional Penanggulangan Terorisme ";
    var kecepatan=150;
    var segarkan=null;
    function bergerak()
    { document.title=txt;
      txt=txt.substring(1,txt.length)+txt.charAt(0);
      segarkan=setTimeout("bergerak()",kecepatan);
    }
    bergerak();
  </script>
</body>
</html>