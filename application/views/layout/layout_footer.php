  </div>
  <!-- /wrapper -->


  <!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg" id="modal-trigger" data-toggle="modal" data-target="#myModal" style="display:none;">
  Launch demo modal
</button>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-context">
      
  </div>
    </div>
  </div>
</div>
    <!-- jQuery -->
    <script src="assets/sb/plugins/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="assets/sb/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
   <script src="assets/sb/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
  <script src="assets/dist/js/sweetalert2.min.js"></script>
  <script src="assets/dist/js/js.js"></script>
  <script src="assets/dist/js/delete_files.js"></script>
  <script src="assets/dist/js/operation.js"></script>
  <script src="assets/dist/js/workflow.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="assets/sb/js/sb-admin-2.js"></script>
    <script src="assets/sb/js/bootstrap-select.min.js"></script>
    
</body>
</html>
