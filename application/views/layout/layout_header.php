
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="<?=base_url();?>">
     <title>BNPT || Badan Nasional Penanggulangan Terorisme </title>
    <!-- Bootstrap Core CSS -->
    <link href="assets/sb/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="assets/sb/css/sb-admin-2.css" rel="stylesheet">
     <link href="assets/sb/css/bootstrap-select.min.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="assets/sb/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="assets/dist/css/sweetalert2.min.css" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
      <style>
      .dropdown-submenu {
    position: relative;
}

.dropdown-submenu .dropdown-menu {
    top: 0;
    left: 100%;
    margin-top: -1px;
}
  .hide {
    display: none;
  }

  .loader {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('assets/img/loading.gif') 50% 50% no-repeat rgb(10, 10, 10);
    opacity: 0.7;
    display: none;
  }
  .form-circle{
    border-radius: 50px;
  }

  
  </style>
</head>
<body>
    <div class="hide" id="homeFolder" data-name="<?=$this->session->userdata('homeFolder')?>"></div>
    <div class="loader"></div>
 <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="dashboard">Menu Utama</a>
            </div>
            <!-- /.navbar-header -->
            <ul class="nav navbar-top-links navbar-left">
                <li><a href="context/mine/myfiles">Dokumen Pribadi</a></li>
                <li><a href="context/mine/shared">Dokumen Bersama</a></li>
                <li><a href="context/mine/repository">Dokumen TU DE II</a></li>
                <li><a href="document/workflow">Workflow</a></li>
            </ul>
            <form action="" method="POST" class="navbar-form navbar-right" style="margin-right: 0 !important">
              <div class="input-group">
                <span class="input-group-addon form-circle"><i class="fa fa-search"></i></span>
                <input type="text" class="form-control form-circle">
              </div>
            </form>
            <!-- /.navbar-top-links --> 
        </nav>
        <!-- Navigation -->

            
      
       