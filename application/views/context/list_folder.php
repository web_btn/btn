 <?php
 $this->load->view('layout/layout_sidebar');
 ?>
	<div class="media" style="margin-top: 0px; padding: 10px;">
		<div class="media-left media-middle">
			<a href="">
				<img class="media-object" src="assets/img/LOGO-BNPT.png" width="50px">
			</a>
		</div>
		<div class="media-body" style="vertical-align: middle;">
			<h3 class="media-heading" style="font-family: 'Lato Light';">Folder Bersama</h3>
		</div>
	</div>



	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header" style="margin: 10px 0 10px;">
					<ul class="nav nav-pills">
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="">
								Select <i class="fa fa-caret-down"></i>    
							</a>
							<ul class="dropdown-menu">
								<li><a href="" data-type="documents" class="select_files"><i class="fa fa-file-o"></i> Documents</a></li>
								<li><a href="" data-type="folders" class="select_files"><i class="fa fa-folder-o"></i> Folders</a></li>
								<li><a href="#" data-type="all" class="select_files"><i class="fa fa-check-circle-o"></i> All</a></li>
								<li><a href="" data-type="invert" class="select_files"><i class="fa fa-refresh"></i> Invert Selection</a></li>
								<li><a href="" data-type="none" class="select_files"><i class="fa fa-times-circle-o"></i> None</a></li>
							</ul>
						</li>
						<li role="presentation" class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="" role="button" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-plus"></i> Create <i class="fa fa-caret-down"></i>    
							</a>
							<ul class="dropdown-menu">
								<li><a href="" id="create_folder" data-id="<?=$node?>" data-title="Create Folder" class="act-modal"><i class="fa fa-folder-o"></i> Folder</a></li>
							<!-- <li><a href=""><i class="fa fa-file-text-o"></i> Plain Text</a></li>
							<li><a href=""><i class="fa fa-file-text-o"></i> HTML</a></li>
							<li><a href=""><i class="fa fa-file-code-o"></i> XML</a></li>
							<li><a href="">Google Docs Document...</a></li>
							<li><a href="">Google Docs Document...</a></li>
							<li><a href="">Google Docs Document...</a></li> -->
							<li class="divider"></li>
							<li class="dropdown-submenu">
								<a class="test" tabindex="-1" href="#">Create document from template <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<?php
                                    foreach ($template['items'] as $key) {
                                        ?>
										<li><a tabindex="-1" href="" data-node="<?=$key['node']['nodeRef']?>" data-id="<?=$node?>" data-title="<?=$key['node']['properties']['cm:name']?>" class="create_template"><?=$key['node']['properties']['cm:name']?></a></li>
										<?php
                                    } ?>

									</ul>
								</li>
							</ul>
						</li>
						<li role="presentation" class="dropdown">
							<a href="" id="upload_file" data-id="<?=$node?>" data-title="Upload File" class="act-modal">
								<i class="fa fa-upload"></i> Upload
							</a>
						</li>
						
						<li role="presentation" class="dropdown pull-right">
							<select class="selectpicker" data-width="fit" id="sortField">
								<option value="cm:name">Name</option>
								<option value="popularity">Popularity</option>
								<option value="cm:title">Title</option>
								<option value="cm:description">Description</option>
								<option value="cm:created">Created</option>
								<option value="cm:creator">Creator</option>
								<option value="cm:modified">Modified</option>
								<option value="cm:modifier">Modifier</option>
								<option value="size">Size</option>
								<option value="mimetype">Mimetype</option>
								<option value="type">Type</option>
								
							</select>
						</li>
						<li role="presentation" class="dropdown" id="selected">
							<a class="dropdown-toggle" id="selected-item" data-toggle="dropdown" href="" role="button" aria-haspopup="true" aria-expanded="false">
								Selected Item <i class="fa fa-caret-down"></i>
							</a>
							<ul class="dropdown-menu">
								<li><a href="" class="copy-to" data-name="Move" data-type="move"><i class="fa fa-scissors "></i> Move to..</a></li>
								<li><a href="" class="copy-to" data-name="Copy" data-type="copy"><i class="fa fa-files-o"></i> Copy to..</a></li>
								<li><a href="" id="multiple-delete"><i class="fa fa-trash-o"></i> Delete</a></li>
								
							</ul>
						</li>
						<li role="presentation" class="pull-right pull-sortAsc">
							<?php
                            if ($sort == "true") {
                                ?>
								<a href="" data-toggle="tooltip" data-placement="left" title="Sort Ascending" id="sortAsc" data-type="asc"> 
									<i class="fa fa-sort-amount-asc"></i>
								</a>
								<?php
                            } elseif ($sort == "false") {
                                ?>
								
								<a href="" data-toggle="tooltip" data-placement="left" title="Sort Descending" id="sortAsc" data-type="desc"> 
									<i class="fa fa-sort-amount-desc"></i>
								</a>
								<?php
                            }
                            ?>
						</li>
					</ul>
				</div>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
		<div class="row list-folder">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<i class="fa fa-folder-open-o"></i> | Repository <br><br>
						<div class="table-responsive">
							<table class="table table-bordered">
								<?php
                                foreach ($data['items'] as $key) {
                                    ?>
									<?php
                                    $url = $key['webdavUrl'];
                                    $url = str_replace("/webdav", "", "$url");
                                    $url = urlencode($url); ?>
									<?php
                                    if ($key['node']['type'] == "cm:folder") {
                                        $type = "folder";
                                    } else {
                                        $type = "file";
                                    } ?>
										<tr>
											<td>
												<div class="checkbox">
													<label>
														<input type="checkbox" name="fileChecked" class="<?=$type?>" value="<?=$key['node']['nodeRef']?>" data-name="<?=$key['node']['properties']['cm:name']?>">
													</label>
												</div>
												<p align="center">

													<?php
                                                    if ($key['node']['type'] == "cm:folder") {
                                                        $doc = current_url()."?path=$url"; ?>
															<a href="<?=$doc?>">
																<img width="100px" height="100px" src="assets/img/folder-icon.png"></a>


																<?php
                                                    } else {
                                                        $doc = base_url("context/mine/document_details?nodeRef=".$key['node']['nodeRef'].""); ?>
																<a href="<?=$doc?>">
																	<img src="<?=$this->API."/api/node/workspace/SpacesStore/".getNode($key['node']['nodeRef'])."/content/thumbnails/doclib?alf_ticket=".$this->ticket."&ph=true"; ?>"></a>
																	<?php
                                                    } ?>
																</p>
															</td>
															<td>
																<a href="<?=$doc?>"><h4><?=$key['node']['properties']['cm:name']?><?php

                                                                if (isset($key['node']['properties']['cm:title'])) {
                                                                    if (!$key['node']['properties']['cm:title'] == null) {
                                                                        echo " <font color='#bbbbbb'>(".$key['node']['properties']['cm:title'].") </font>";
                                                                    }
                                                                } ?></h4></a>
																		<p>Modified <?=time_elapsed_string($key['node']['properties']['cm:modified']['value'])?> by <?=$key['node']['properties']['cm:modifier']['displayName']?></p>


																		<?php

                                                                        if (isset($key['node']['properties']['cm:description'])) {
                                                                            if ($key['node']['properties']['cm:description'] == null) {
                                                                                echo "<p style='color:#bbb;'>No Description</p>";
                                                                            } else {
                                                                                echo $key['node']['properties']['cm:description'];
                                                                            }
                                                                        } else {
                                                                            echo "<p style='color:#bbb;'>No Description</p>";
                                                                        } ?>

																				<p style="color:#bbb;">No Tags</p>
																				<i class="fa fa-star" style="color:#bbb;"></i> Favorite |  
																				<i class="fa fa-thumbs-o-up" style="color:#bbb;"></i> Like 
																				<span style="background-color: #bbb; ">0</span>
																			</td>
																			<td class="more">
																				<div class="detail">
																					<?php
                                                                                    if ($key['node']['type'] != "cm:folder") {
                                                                                        ?>
																						<!-- Selain Folder -->
																						<!-- <p><a href=""><span class="fa fa-star" style="color:#bbb;"></span> View Details</a></p> -->
																						<p><a href="<?=$this->API.$key['node']['contentURL']."?a=true"; ?>" target="_blank"><span class="fa fa-download" style="color:#bbb;"></span> Download</a></p>
																						<?php
                                                                                        if ($key['node']['mimetype'] == "application/pdf") {
                                                                                            ?>
																							<p><a href="<?=$this->API."/api/node/workspace/SpacesStore/".getNode($key['node']['nodeRef'])."/content"; ?>" target="_blank"><span class="fa fa-file-pdf-o" style="color:#bbb;"></span> View Printable PDF</a></p>
																							<?php
                                                                                        } else {
                                                                                            ?>
																							<p><a href="<?=$this->API."/api/node/workspace/SpacesStore/".getNode($key['node']['nodeRef'])."/content/thumbnails/pdf?c=force"; ?>" target="_blank"><span class="fa fa-file-pdf-o" style="color:#bbb;"></span> View Printable PDF</a></p>

																							<?php
                                                                                        } ?>
                                                                                        <p><a href="" data-type="copy" data-content="document" data-node="<?=$key['node']['nodeRef']?>" data-name="Copy Document : <?=$key['node']['properties']['cm:name']?>" data-title="<?=$key['node']['properties']['cm:name']?>" class="copy-to-detail"><i class="fa fa-files-o"></i> Copy to..</a></p>
																							<p><a href="" data-type="move" data-content="document" data-node="<?=$key['node']['nodeRef']?>" data-name="Move Document : <?=$key['node']['properties']['cm:name']?>" data-title="<?=$key['node']['properties']['cm:name']?>" class="copy-to-detail"><i class="fa fa-scissors"></i> Move to..</a></p>
																							
																						<?php
                                                                                    } else {
                                                                                        ?>
																							<!-- Untuk Folder -->
																							<p><a href="" data-type="copy" data-content="folder" data-node="<?=$key['node']['nodeRef']?>" data-name="Copy Folder : <?=$key['node']['properties']['cm:name']?>" data-title="<?=$key['node']['properties']['cm:name']?>" class="copy-to-detail"><i class="fa fa-files-o"></i> Copy to..</a></p>
																							<p><a href="" data-type="move" data-content="folder" data-node="<?=$key['node']['nodeRef']?>" data-name="Move Folder : <?=$key['node']['properties']['cm:name']?>" data-title="<?=$key['node']['properties']['cm:name']?>" class="copy-to-detail"><i class="fa fa-scissors"></i> Move to..</a></p>
																							<?php
                                                                                    } ?>
																						<!-- Double File & Folder -->
																						<p><a href="" id="edit_properties" data-id="<?=getNode($key['node']['nodeRef'])?>" data-title="Edit Properties : <?=$key['node']['properties']['cm:name']?>" class="act-modal"><i class="fa fa-pencil-square-o"></i> Edit Properties</a></p>
																					</div>
																				</td>
																			</tr>
																			<?php
                                }
                                                                        ?>
																	</table>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
        <!-- /page-wrapper -->