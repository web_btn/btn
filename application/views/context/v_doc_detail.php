
<div class="media" style="margin-top: 0px; padding: 10px;">
	<div class="media-left media-middle">
		<a href="">
			<img class="media-object" src="assets/img/LOGO-BNPT.png" width="50px">
		</a>
	</div>
	<div class="media-body" style="vertical-align: middle;">
		<h3 class="media-heading" style="font-family: 'Lato Light';">Document Details</h3>
	</div>
</div>

<div id="page-wrapper" style="margin: 0 !important;">
	<br>
	<div class="row">
		<div class="col-sm-12 col-md-9">
			<div class="media">
				<div class="media-left">
					<a href="#">
						<img class="media-object" src="assets/img/dokumen2.png" style="width: 70px">
					</a>
				</div>
				<div class="media-body">
					<h2 class="media-heading"><?=$data['item']['node']['properties']['cm:name']?></h2>
					<p>Modified <?=$data['item']['node']['properties']['cm:modified']['value']?> by <?=$data['item']['node']['properties']['cm:modifier']['displayName']?></p>
				</div>
			</div>


			

			<?php
			if($data['item']['node']['mimetype'] == "application/pdf"){

				?>
				<iframe src="<?=$this->API."/api/node/workspace/SpacesStore/".getNode($data['item']['node']['nodeRef'])."/content";?>" height="650" style="width: 100%;"></iframe>
				<?php
			}
			else{
				?>
				<iframe src="<?=$this->API."/api/node/workspace/SpacesStore/".getNode($data['item']['node']['nodeRef'])."/content/thumbnails/pdf?c=force";?>" height="650" style="width: 100%;"></iframe>

				<?php
			}
			?>
		</div>
	</div>
</div>
