<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Auth extends CI_Model {


    public function doLogin($ticket){
        $data['ticket'] = $ticket;
        $data = (object) $data;
            $this->setLoginStatus(true);
            $this->setUserSession($data);
            return true;
        
    }

    public function isPasswordTrue($db_password,$password){
        return verifyPassword($db_password,$password);
    }

    public function setUserSession($obj){

        $arr = ["user_data"=>$obj,'groups'=>[]];
        $this->session->set_userdata($arr);
        $this->session->set_userdata('random',rand(1000,9999));
    }

    public function setLoginStatus($status = true){
        $this->session->set_userdata(Auth::$prefix."login",true);
    }

    public function logout(){
        $this->session->sess_destroy();
    }

    public function reload (){
        $this->db->where(['id'=>Auth::user()->id]);
        $cek = $this->db->get('user')->row();
        $this->setUserSession($cek);
    }
}