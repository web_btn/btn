<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once "AuthInterface.php";

class Auth extends AltCI implements AuthInterface {

    public static $prefix = "4lT3R_Auth";
    private static $login_url = "auth/login";
    private static $dashboard_url = "dashboard";

    public function __construct() {
        parent::__construct();
    }

    public static function isLogin() {
        if(self::$CI->session->userdata(self::$prefix."login"))
            return true;
        else
            return false;
    }

    public static function user() {
        $arr_sess = self::$CI->session->userdata("user_data");
        return (object)$arr_sess;
    }

    public static function redirectIfNotLogin() {
        if(!self::isLogin())
            redirect(base_url(self::$login_url));
        else{
            if(self::$CI->session->userdata("lock_screen")){
                redirect("lock");
            }
        }
    }



    public static function update() {
        self::$CI->load->model('M_Auth');
        self::$CI->M_Auth->reload();
    }

    public static function updateLastActive(){
        self::$CI->db->where('id',Auth::user()->id);
        self::$CI->db->update('user',['last_login'=>date("Y-m-d H:i:s")]);
    }
}