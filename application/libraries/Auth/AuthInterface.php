<?php
defined('BASEPATH') OR exit('No direct script access allowed');

interface AuthInterface {

    public static function isLogin();
    public static function user();
    public static function redirectIfNotLogin();
    public static function update();

}