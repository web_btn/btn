<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alt_FacadeTemplate extends CI_Controller {
    protected $view_data = [];
    
    public function __construct(){
        parent::__construct();
    }

    protected function view($view=null){
        $this->load->view($this->getData('_main_content'),$this->view_data);
    }

    protected function setPage($page){
        $this->setData('_main_content',$page);
    }

    protected function setData($key,$value){
      $this->view_data[$key] = $value;
    }

    protected function getData($key=null){
        if($key)
            return $this->view_data[$key];
    }

    protected function parentAjax(){
        if (!$this->input->is_ajax_request())
            redirect(base_url());
    }

    protected function inputPost($name,$html_encode=true){
        $field = $this->input->post($name,true);
        if($html_encode){
            return saveInput($field);
        } else {
            return $field;
        }
    }

    protected function inputGet($name,$html_encode=true){
        $field = $this->input->get($name,true);
        if($html_encode){
            return saveInput($field);
        } else {
            return $field;
        }
    }

    protected function jsonResponse($status,$msg,$add=[],$redirect=null){
        if(is_array($msg)){
            $x = '<ul>';
            foreach ($msg as $a){
                $x .= '<li>'.$a.'</li>';
            }
            $x .= '</ul>';
            $data = ['status'=>$status,'msg'=>$x,"redirect"=>$redirect];
        } else {
            $data = ['status'=>$status,'msg'=>$msg,"redirect"=>$redirect];
        }
        if($add)
            $data['opt'] =$add;

        echo json_encode($data);
    }

}