<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH."core/Alt_FacadeTemplate.php";
class Auth extends Alt_FacadeTemplate
{
    public $API ="";

    public function __construct()
    {
        parent::__construct();
        // $this->API="http://localhost/rest_klien/index.php";
        
        
        $this->API="http://10.0.0.175:8080/alfresco/s";
    }

    // menampilkan data kontak
    public function login()
    {

        if ($this->input->post() == null) {
            $this->do_logout();
            $this->load->view('auth/v_index');
        } else {
            $this->do_login();
        }
    }

    public function logout(){
      $ticket = $this->session->userdata('ticket');
      $data = array();
        $curl =  $this->curl->simple_delete($this->API.'/api/login/ticket/'.$ticket.'?alf_ticket='.$ticket.'&format=json', $data, array(CURLOPT_BUFFERSIZE => 50));

        if ($curl) {
            $this->session->sess_destroy();
            echo"<script>alert('Logout Berhasil');window.location.href=('".base_url()."auth/login');</script>";
        } else {
            echo"<script>alert('Gagal Logout');window.location.href=('".base_url()."dashboard');</script>";
        }
    }

    private function do_logout(){
      $ticket = $this->session->userdata('ticket');
      $data = array();
        $curl =  $this->curl->simple_delete($this->API.'/api/login/ticket/'.$ticket.'?alf_ticket='.$ticket.'&format=json', $data, array(CURLOPT_BUFFERSIZE => 50));

        if ($curl) {
            $this->session->sess_destroy();
            
        } 
    }
    private function do_login()
    {
        $data = array(
      'username'      =>  $this->inputPost('username'),
      'password'      =>  $this->inputPost('password'));
        $json = json_encode($data);
        $this->curl->http_header('Content-Type: application/json');
        $login =  $this->curl->simple_post($this->API.'/api/login', $json, array(CURLOPT_BUFFERSIZE => 50));

        if ($login) {
            $data = json_decode($this->curl->simple_post($this->API.'/api/login', $json, array(CURLOPT_BUFFERSIZE => 50)), true);
           
            $this->session->set_userdata('is_login', true);
            $this->session->set_userdata('ticket', $data['data']['ticket']);
            $this->session->set_userdata('username',$this->inputPost('username'));
            $ticket = $this->session->userdata('ticket');
             $data2 = json_decode($this->curl->simple_get($this->API.'/custom/bnpt/userhome?alf_ticket='.$ticket.''), true);
             $this->session->set_userdata('homeFolder', $data2['data']['homeFolder']);
            echo"<script>alert('Login Berhasil');window.location.href=('".base_url()."dashboard');</script>";
        } else {
            echo"<script>alert('Username atau password salah');window.location.href=('".base_url()."auth/login');</script>";
        }
    }
}
