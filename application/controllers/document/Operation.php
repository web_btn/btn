<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH."core/Alt_FacadeTemplate.php";
class Operation extends Alt_FacadeTemplate
{
	public $API ="";
	public $ticket="";
	public function __construct()
	{
		parent::__construct();
        // $this->API="http://localhost/rest_klien/index.php";


		$this->ticket = $this->session->userdata('ticket');
		$this->API="http://10.0.0.175:8080/alfresco/s";
	}

	public function edit_properties()
	{

		if ($this->input->post() == null) {
			$node = $_GET['nodeRef'];
        $this->curl->http_header('Content-Type: application/json');
        $detail = json_decode($this->curl->simple_get($this->API."/slingshot/doclib2/node/workspace/SpacesStore/$node?alf_ticket=".$this->ticket.""),true);
        $data['detail'] = $detail;
			$this->load->view('layout/layout_header');
			$this->load->view('document/v_edit_properties',$data);
			$this->load->view('layout/layout_footer');
		} else {
			$this->curl->http_header('Content-Type: application/json');
			$data['properties'] = array('cm:name'=>$this->inputPost('name'),'cm:title'=>$this->inputPost('title'),'cm:description'=>$this->inputPost('description'));
			$json = json_encode($data);
			$node = $this->inputPost('node');
			$curl =  $this->curl->simple_post($this->API."/api/metadata/node/workspace/SpacesStore/$node?alf_ticket=".$this->ticket."", $json, array(CURLOPT_BUFFERSIZE => 50));
			if ($curl) {
				$this->jsonResponse(true, "Berhasil Mengedit");
			} else {
				$this->jsonResponse(false, "Gagal Mengedit");
			}
		}
	}

	public function create_folder()
	{
		

		if ($this->input->post() == null) {
			$this->load->view('layout/layout_header');

			$this->load->view('document/v_create_folder');
			$this->load->view('layout/layout_footer');
		} else {
			$this->curl->http_header('Content-Type: application/json');
			$data = array('name'=>$this->inputPost('name'),'title'=>$this->inputPost('title'),'description'=>$this->inputPost('description'));
			$json = json_encode($data);
			$node = getNode($this->inputPost('node'));
			$curl =  $this->curl->simple_post($this->API."/api/node/folder/workspace/SpacesStore/$node?alf_ticket=".$this->ticket."", $json, array(CURLOPT_BUFFERSIZE => 50));
			if ($curl) {
				$this->jsonResponse(true, "Berhasil Membuat Folder $data[name]");
			} else {
				$this->jsonResponse(false, "Gagal Membuat Folder $data[name]");
			}
		}
	}

	public function copy_file(){
		$node = $this->inputPost('node');
		$path = getNode($this->inputPost('id'));
		$title = $this->inputPost('title');
		$data['nodeRefs'] = array($node);
		$json = json_encode($data);
		$this->curl->http_header('Content-Type: application/json');
		$curl =  $this->curl->simple_post($this->API."/slingshot/doclib/action/copy-to/node/workspace/SpacesStore/$path?alf_ticket=".$this->ticket."", $json, array(CURLOPT_BUFFERSIZE => 50));
		if ($curl) {
			$this->jsonResponse(true, "Berhasil Membuat File Template <b>$title</b>");
		} else {
			$this->jsonResponse(false, "Gagal Membuat File Template <b>$title</b>");
		}
		
	}

	public function copy_to(){
		if ($this->input->post() == null) {
			
			$this->load->view('layout/layout_header');
			$this->load->view('document/v_copy_move');
			$this->load->view('layout/layout_footer');
		} else {

			$ex = explode(",", $_POST['folder']);
			$ex2 = explode(",", $_POST['file']);
			$node = array();
			for ($i=0; $i < count($ex) ; $i++) { 
				
				array_push($node, $ex[$i]);
			}
			for ($i=0; $i < count($ex2) ; $i++) { 
				
				array_push($node, $ex2[$i]);
			}
			$path = getNode($this->inputPost('path'));

			$data['nodeRefs'] = $node;
			$json = json_encode($data);
			$this->curl->http_header('Content-Type: application/json');
			$curl =  $this->curl->simple_post($this->API."/slingshot/doclib/action/copy-to/node/workspace/SpacesStore/$path?alf_ticket=".$this->ticket."", $json, array(CURLOPT_BUFFERSIZE => 50));
			if ($curl) {
				$this->jsonResponse(true, "Berhasil Duplikat Data");
			} else {
				$this->jsonResponse(false, "Gagal Duplikat Data");
			}
		}
	}

	public function move_to(){
		if ($this->input->post() == null) {
			
			$this->load->view('layout/layout_header');
			$this->load->view('document/v_copy_move');
			$this->load->view('layout/layout_footer');
		} else {

			$ex = explode(",", $_POST['folder']);
			$ex2 = explode(",", $_POST['file']);
			$node = array();
			for ($i=0; $i < count($ex) ; $i++) { 
				
				array_push($node, $ex[$i]);
			}
			for ($i=0; $i < count($ex2) ; $i++) { 
				
				array_push($node, $ex2[$i]);
			}
			$path = getNode($this->inputPost('path'));

			$data['nodeRefs'] = $node;
			$json = json_encode($data);
			$this->curl->http_header('Content-Type: application/json');
			$curl =  $this->curl->simple_post($this->API."/slingshot/doclib/action/move-to/node/workspace/SpacesStore/$path?alf_ticket=".$this->ticket."", $json, array(CURLOPT_BUFFERSIZE => 50));
			if ($curl) {
				$this->jsonResponse(true, "Berhasil Memindahkan Data");
			} else {
				$this->jsonResponse(false, "Gagal Memindahkan Data");
			}
		}
	}

	public function upload_file(){
		if ($this->input->post() == null) {
			$this->load->view('layout/layout_header');
			$this->load->view('document/v_upload_file');
			$this->load->view('layout/layout_footer');
		} else {
			$path = $this->inputPost('node');
			$temp = $_FILES['filedata']['tmp_name'];
			$title = $_FILES['filedata']['name'];

			$data = array('filedata'=>$temp,'filename'=>$title,'destination'=>$path,'overwrite'=>'false');

		// $this->curl->http_header('Content-Type: application/x-www-form-urlencoded');
			$curl =  $this->curl->simple_post($this->API."/api/upload?alf_ticket=".$this->ticket."", $data, array(CURLOPT_BUFFERSIZE => 50));
			if ($curl) {
				$this->jsonResponse(true, "Berhasil Mengupload File <b>$title</b>");
			} else {
				$this->jsonResponse(false, "Gagal Mengupload File <b>$title</b>");
			}

			
		}
		
	}

	public function delete_multiple(){

		if ($this->input->post() == null) {
			
			$this->load->view('layout/layout_header');
			$this->load->view('document/v_confirm_delete');
			$this->load->view('layout/layout_footer');
		} else {
			
			

			if(isset($_POST['file']))
			{
				$this->curl->http_header('Content-Type: application/json');
				$ex2 = explode(",", $_POST['file']);
				$file = array();
				for ($i=0; $i < count($ex2) ; $i++) { 

					array_push($file, $ex2[$i]);
				}

				$data2['nodeRefs'] = $file;
				$json2 = json_encode($data2);
				$curl2 =  $this->curl->simple_delete($this->API."/slingshot/doclib/action/files?alf_ticket=".$this->ticket."", $json2);
			}

			if(isset($_POST['folder']))
			{
				$this->curl->http_header('Content-Type: application/json');
				$ex = explode(",", $_POST['folder']);
				$folder = array();
				for ($i=0; $i < count($ex) ; $i++) { 

					array_push($folder, $ex[$i]);
				}

				$data['nodeRefs'] = $folder;
				$json = json_encode($data);
				$curl =  $this->curl->simple_delete($this->API."/slingshot/doclib/action/folders?alf_ticket=".$this->ticket."", $json);
			}
			if ($curl2 && $curl) {
				$this->jsonResponse(true, "Berhasil Dihapus");
			} else {
				$this->jsonResponse(false, "Gagal Dihapus");
			}
		}
	}


}
