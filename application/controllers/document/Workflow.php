<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH."core/Alt_FacadeTemplate.php";
include_once(APPPATH."third_party/PhpWord/Autoloader.php");

use PhpOffice\PhpWord\Autoloader;
use PhpOffice\PhpWord\Settings;

Autoloader::register();
Settings::loadConfig();
class Workflow extends Alt_FacadeTemplate
{
    public $API ="";
    public $ticket="";
    public function __construct()
    {
        parent::__construct();
        // $this->API="http://localhost/rest_klien/index.php";


        $this->ticket = $this->session->userdata('ticket');
        $this->API="http://10.0.0.175:8080/alfresco/s";
    }

    public function index()
    {
        $this->load->view('layout/layout_header');
        $this->load->view("workflow/v_workflow");
        $this->load->view('layout/layout_footer');
    }

    public function start()
    {
        $view = $_GET['view'];
        if ($this->input->post() == null) {
            $this->load->view('layout/layout_header');
            $this->load->view("workflow/v_$view");
            $this->load->view('layout/layout_footer');
        } else {
        }
    }

    public function generate()
    {
        if ($this->input->post() != null) {
            $file = $this->input->post("file");
            $PHPWord = new \PhpOffice\PhpWord\PhpWord();
            $template = new \PhpOffice\PhpWord\TemplateProcessor("./assets/template/$file.docx");
            foreach ($_POST as $key => $value) {
                $template->setValue($key, $value);
                // $template->setValue(strip_tags($key), $value);
            }
            // menyimpan hasil
            $file_hasil ="./assets/template/temp/$file.docx";
            $template->saveAs($file_hasil);
            header('location: ' .base_url($file_hasil));// download file
        }
        else{
            redirect(base_url());
        }
    }
}
