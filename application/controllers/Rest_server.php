<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Rest_server extends CI_Controller
{
    public $API ="";

    public function __construct()
    {
        parent::__construct();
        $this->API="http://localhost/rest_klien/";
        // $this->API="http://10.0.0.175:8080/alfresco/s";
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->helper('form');
        $this->load->helper('url');
    }
    
    // menampilkan data kontak
    public function index()
    {
        $data['data'] = json_decode($this->curl->simple_get($this->API.'/mahasiswa'));
        $this->load->view('mahasiswa/list', $data);
    }
    
    // insert data kontak
    public function create()
    {
        if (isset($_POST['submit'])) {
            $data = array(
                'nim'       =>  $this->input->post('nim'),
                'nama'      =>  $this->input->post('nama'),
                'id_jurusan'      =>  $this->input->post('id_jurusan'),
                'alamat'=>  $this->input->post('alamat'));
            $insert =  $this->curl->simple_post($this->API.'/mahasiswa', $data, array(CURLOPT_BUFFERSIZE => 10));
            if ($insert) {
                $this->session->set_flashdata('hasil', 'Insert Data Berhasil');
            } else {
                $this->session->set_flashdata('hasil', 'Insert Data Gagal');
                $this->curl->debug();
            }
            redirect('rest_server');
        } else {
            $this->load->view('mahasiswa/create');
        }
    }
        
    // edit data kontak
    public function edit()
    {
        if (isset($_POST['submit'])) {
            $data = array(
                    'id'       =>  $this->input->post('id'),
                    'nama'      =>  $this->input->post('nama'),
                    'nomor'=>  $this->input->post('nomor'));
            $update =  $this->curl->simple_put($this->API.'/kontak', $data, array(CURLOPT_BUFFERSIZE => 10));
            if ($update) {
                $this->session->set_flashdata('hasil', 'Update Data Berhasil');
            } else {
                $this->session->set_flashdata('hasil', 'Update Data Gagal');
            }
            redirect('kontak');
        } else {
            $params = array('id'=>  $this->uri->segment(3));
            $data['datakontak'] = json_decode($this->curl->simple_get($this->API.'/kontak', $params));
            $this->load->view('kontak/edit', $data);
        }
    }
            
    // delete data kontak
    public function delete($id)
    {
        if (empty($id)) {
            redirect('rest_server');
        } else {
            $delete =  $this->curl->simple_delete($this->API.'/mahasiswa', array('nim'=>$id), array(CURLOPT_BUFFERSIZE => 10));
            if ($delete) {
                $this->session->set_flashdata('hasil', 'Delete Data Berhasil');
            } else {
                $this->session->set_flashdata('hasil', 'Delete Data Gagal');
            }
            redirect('rest_server');
        }
    }
            
    public function login()
    {
        $data = array(
                    'username'      =>  "Dian.Eko.Rini",
                    'password'      =>  "Dian.Eko.Rini");
        $json = json_encode($data);
        $update =  $this->curl->simple_post($this->API.'/api/login', $json, array(CURLOPT_BUFFERSIZE => 50));
                    
        if ($update) {
            echo"<script>alert('berhasil');</script>";
            $data = json_decode($this->curl->simple_post($this->API.'/api/login', $json, array(CURLOPT_BUFFERSIZE => 50)));
            foreach ($data as $key) {
                foreach ($key as $key2 => $value) {
                    echo "$key2 : $value";
                }
            }
        } else {
            echo"<script>alert('gagal');</script>";
        }
    }
}
