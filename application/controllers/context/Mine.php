<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH."core/Alt_FacadeTemplate.php";
class Mine extends Alt_FacadeTemplate
{
    public $API ="";
    public $ticket="";
    public function __construct()
    {
        parent::__construct();
        // $this->API="http://localhost/rest_klien/index.php";
        

        $this->ticket = $this->session->userdata('ticket');
        $this->API="http://10.0.0.175:8080/alfresco/s";
    }

    
    public function repository()
    {
        $path = null;
        if(isset($_GET['path'])){
            $path = urldecode($_GET['path']);
            $path = str_replace(" ","%20",$path);
        }
        $sortField = "cm:name";
        if(isset($_GET['sortField']))
            $sortField = $_GET['sortField'];
        $sortAsc = "true";
        if(isset($_GET['sortAsc']))
            $sortAsc = $_GET['sortAsc'];



        $this->curl->http_header('Content-Type: application/json');
        $folder = json_decode($this->curl->simple_get($this->API."/slingshot/doclib2/doclist/folders/node/alfresco/company/home$path?alf_ticket=".$this->ticket."&sortField=$sortField&sortAsc=$sortAsc"),true);
        $file = json_decode($this->curl->simple_get($this->API."/slingshot/doclib2/doclist/documents/node/alfresco/company/home$path?alf_ticket=".$this->ticket."&sortField=$sortField&sortAsc=$sortAsc"),true);
        $data['template'] = json_decode($this->curl->simple_get($this->API.'/slingshot/doclib2/doclist/documents/node/alfresco/company/home/Data%20Dictionary/Node%20Templates?alf_ticket='.$this->ticket),true);
        if(!$folder || !$file)
        {
            redirect('auth/login');
        }
        $data['data'] = array_merge_recursive($folder,$file);
        $list = $folder;
        $data['node'] = $list['metadata']['parent']['nodeRef'];
        $data['sort'] = $sortAsc;
        $this->load->view('layout/layout_header');
        $this->load->view('context/list_folder',$data);
        $this->load->view('layout/layout_footer');
    }

    public function shared()
    {
        $path = "/Shared";
        if(isset($_GET['path'])){
            $path = urldecode($_GET['path']);
            $path = str_replace(" ","%20",$path);
        }
        $sortField = "cm:name";
        if(isset($_GET['sortField']))
            $sortField = $_GET['sortField'];
        $sortAsc = "true";
        if(isset($_GET['sortAsc']))
            $sortAsc = $_GET['sortAsc'];



        $this->curl->http_header('Content-Type: application/json');
        $folder = json_decode($this->curl->simple_get($this->API."/slingshot/doclib2/doclist/folders/node/alfresco/company/home$path?alf_ticket=".$this->ticket."&sortField=$sortField&sortAsc=$sortAsc"),true);
        $file = json_decode($this->curl->simple_get($this->API."/slingshot/doclib2/doclist/documents/node/alfresco/company/home$path?alf_ticket=".$this->ticket."&sortField=$sortField&sortAsc=$sortAsc"),true);
        $data['template'] = json_decode($this->curl->simple_get($this->API.'/slingshot/doclib2/doclist/documents/node/alfresco/company/home/Data%20Dictionary/Node%20Templates?alf_ticket='.$this->ticket),true);
        if(!$folder || !$file)
        {
            redirect('auth/login');
        }
        $data['data'] = array_merge_recursive($folder,$file);
        $list = $folder;
        $data['node'] = $list['metadata']['parent']['nodeRef'];
        $data['sort'] = $sortAsc;
        $this->load->view('layout/layout_header');
        $this->load->view('context/list_folder',$data);
        $this->load->view('layout/layout_footer');
    }

    public function myfiles()
    {
        $path = null;
        if(isset($_GET['path'])){
            $path = urldecode($_GET['path']);
            $path = str_replace(" ","%20",$path);
        }
        $sortField = "cm:name";
        if(isset($_GET['sortField']))
            $sortField = $_GET['sortField'];
        $sortAsc = "true";
        if(isset($_GET['sortAsc']))
            $sortAsc = $_GET['sortAsc'];



        $this->curl->http_header('Content-Type: application/json');
        if(isset($_GET['path'])){
            $folder = json_decode($this->curl->simple_get($this->API."/slingshot/doclib2/doclist/folders/node/alfresco/company/home$path?alf_ticket=".$this->ticket."&sortField=$sortField&sortAsc=$sortAsc"),true);
            $file = json_decode($this->curl->simple_get($this->API."/slingshot/doclib2/doclist/documents/node/alfresco/company/home$path?alf_ticket=".$this->ticket."&sortField=$sortField&sortAsc=$sortAsc"),true);
        }else{
            $folder = json_decode($this->curl->simple_get($this->API."/slingshot/doclib2/doclist/folders/node/alfresco/company/home/User%20Homes/".$this->session->userdata('homeFolder')."?alf_ticket=".$this->ticket."&sortField=$sortField&sortAsc=$sortAsc"),true);
            $file = json_decode($this->curl->simple_get($this->API."/slingshot/doclib2/doclist/documents/node/alfresco/company/home/User%20Homes/".$this->session->userdata('homeFolder')."?alf_ticket=".$this->ticket."&sortField=$sortField&sortAsc=$sortAsc"),true);
        }
        $data['template'] = json_decode($this->curl->simple_get($this->API.'/slingshot/doclib2/doclist/documents/node/alfresco/company/home/Data%20Dictionary/Node%20Templates?alf_ticket='.$this->ticket),true);
        if(!$folder || !$file)
        {
            redirect('auth/login');
        }
        $data['data'] = array_merge_recursive($folder,$file);
        $list = $folder;
        $data['node'] = $list['metadata']['parent']['nodeRef'];
        $data['sort'] = $sortAsc;
        $this->load->view('layout/layout_header');
        $this->load->view('context/list_folder',$data);
        $this->load->view('layout/layout_footer');
    }

    public function document_details()
    {

        $node = getNode($_GET['nodeRef']);
        $this->curl->http_header('Content-Type: application/json');
        $detail = json_decode($this->curl->simple_get($this->API."/slingshot/doclib2/node/workspace/SpacesStore/$node?alf_ticket=".$this->ticket.""),true);

        if(!$detail)
        {
            redirect('auth/login');
        }
        $data['data'] = $detail;

        $this->load->view('layout/layout_header');
        $this->load->view('context/v_doc_detail',$data);
        $this->load->view('layout/layout_footer');
    }

    public function pdf()
    {

        echo"<iframe style='width:100%;height:100%;' src='http://10.0.0.175:8080/alfresco/s/api/node/workspace/SpacesStore/af81402b-5226-4f1a-8320-5ce944b8d622/content'></iframe>";
    }
    public function test()
    {
        $path = null;
        
        $folder = json_decode($this->curl->simple_get($this->API.'/slingshot/doclib2/doclist/folders/node/alfresco/company/home'.$path.'?alf_ticket='.$this->ticket),true);
        if(!$folder)
        {
            redirect('auth/login');
        }
        $data['data'] = $folder;

        $this->load->view('test',$data);
        // $list = $folder;
        // $data['node'] = $list['metadata']['parent']['nodeRef'];
        // echo "<ul>";
        // foreach ($list['items'] as $key) {
        //     echo "<li>";
        //     echo $key['node']['properties']['cm:name'];
        //         $url = $key['webdavUrl'];
        //         $url = str_replace("/webdav", "", "$url");
        //         $path = str_replace(" ","%20",$url);

        //      $folder = json_decode($this->curl->simple_get($this->API.'/slingshot/doclib2/doclist/folders/node/alfresco/company/home'.$path.'?alf_ticket='.$this->ticket),true);
        //      $no = 0;
        //     foreach ($folder['items'] as $key2) {
        //         if($no==0) echo"<ul>";
        //         echo "<li>";
        //     echo $key2['node']['properties']['cm:name'];
        //     echo "</li>";
        //      $url = $key2['webdavUrl'];
        //         $url = str_replace("/webdav", "", "$url");
        //         $path = str_replace(" ","%20",$url);
        //      $folder2 = json_decode($this->curl->simple_get($this->API.'/slingshot/doclib2/doclist/folders/node/alfresco/company/home'.$path.'?alf_ticket='.$this->ticket),true);
        //      $no2 = 0;
        //     foreach ($folder2['items'] as $key2) {
        //         if($no2==0) echo"<ul>";
        //         echo "<li>";
        //     echo $key2['node']['properties']['cm:name'];
        //     echo "</li>";
        //     $total2 = count($folder2['items'])-1;
        //        if($no2==$total2) echo"</ul>";
        //         $no2++;
        //     }

        //     $total = count($folder['items'])-1;
        //        if($no==$total) echo"</ul>";
        //         $no++;
        //     }
        //       echo "</li>";

        // }
        // echo"</ul>";
    }

    public function test2()
    {
        $path = $_POST['path'];
        $path = str_replace(" ","%20",$path);

        $folder = json_decode($this->curl->simple_get($this->API.'/slingshot/doclib2/doclist/folders/node/alfresco/company/home'.$path.'?alf_ticket='.$this->ticket),true);
        $data = $folder;
        echo "<ul class='list-destination'>";
        foreach ($data['items'] as $key) {
            $url = $key['webdavUrl'];
            $url = str_replace("/webdav", "", "$url");
           $id = AltUuid::generateRandomString();
        echo "<li class='list-item' id='$id'><a href class='list' data-path='$url' data-id='$id' data-node='".$key['node']['nodeRef']."'><i class='fa fa-folder'></i> ";
            echo $key['node']['properties']['cm:name'];

            echo "</a></li>";
        }
        echo "</ul>";
       
    }

    
}
