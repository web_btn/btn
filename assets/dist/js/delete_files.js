$(document).ready(function() {
  
  function triggerFormDelete(url,judul,folder,file,folder_name,file_name, selector=".form-ajax") {
    setLoading(true);

    $(".modal-title").html(judul);
    $(".modal-context").load(url+" "+selector,function(response,status){
      setLoading(false);
      for (var i = 0; i < folder_name.length; i++) {
       $('.nodeRefs').append("<a href='#'' class='list-group-item'><span class='glyphicon glyphicon-folder-open' aria-hidden='true'></span>&nbsp;&nbsp;"+folder_name[i]+"</a>");
     }
     for (var i = 0; i < file_name.length; i++) {
       $('.nodeRefs').append("<a href='#'' class='list-group-item'><span class='fa fa-file' aria-hidden='true'></span>&nbsp;&nbsp;"+file_name[i]+"</a>");
     }
     
     $("form#form-input").submit(function() {
      if ($(this).valid()) {

        url = $(this).attr('action');

        var data = new FormData($(this)[0]);
        el = $(this);
        swal({
          title: 'Apakah Anda sudah yakin?',
          text: "Data akan dihapus",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya, hapus data!'
        }).then(function () {
          setLoading(true);
          $.ajax({
           url: url,             
           type: "POST",
           dataType: "JSON",
           data: 'folder='+folder+'&file='+file,
           success: function(data) {
            $(".loader").css("display", "none");
            if (data.status) {
              swal(
                'Berhasil!',
                data.msg,
                'success'
                );
             refreshContent();
            } else {
              swal(
                'Gagal',
                data.msg,
                'error'
                )
            }
          },
          error: function(xhr, status) {
           setLoading(false);
           console.log(xhr);
           swal(
            'Gagal',
            'Data Gagal Dihapus',
            'error'
            )
         }
       });
        }).catch(swal.noop);
      }
      return false;
    });
   });
    $("#modal-trigger").trigger("click");

  }
  $("#multiple-delete").on("click",function (e) {
    e.preventDefault();
    var base_url = $(this).attr("href");
    var judul = "Confirm Multiple Delete";
    var url = base_url+"document/operation/delete_multiple";
    folder = $('input[name="fileChecked"][class="folder"]:checked').map(function(){
      return $(this).val();
    }).get();
    var file = $('input[name="fileChecked"][class="file"]:checked').map(function(){
      return $(this).val();
    }).get();
    folder_name = $('input[name="fileChecked"][class="folder"]:checked').map(function(){
      return $(this).data('name');
    }).get();
    var file_name = $('input[name="fileChecked"][class="file"]:checked').map(function(){
      return $(this).data('name');
    }).get();


    triggerFormDelete(url,judul,folder,file,folder_name,file_name);
    return false;
  });

});




