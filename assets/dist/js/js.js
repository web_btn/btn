
  function checkSelected(){
    var check = $('input[name="fileChecked"]:checked').length;
    if (check>0) {
      $('#selected').removeClass('disabled');
      $('#selected-item').removeClass('disabled');
    } else {
      $('#selected').addClass('disabled');
      $('#selected-item').addClass('disabled');

    }
  }

  function refreshContent(sortField=null,sortAsc="true"){
    setLoading(true);
    var selector = ".list-folder";
    if(sortField == null && sortAsc == "true")
    {
      var url = $(location).attr('href');
    }
    else{
      var cek = $(location).attr('search');
      var url2 = $(location).attr('href');
      if(cek == "")
      {      
        var url = url2+"?sortField="+sortField+"&sortAsc="+sortAsc;
      }
      else{
        var url = url2+"&sortField="+sortField+"&sortAsc="+sortAsc;
      }
    }
    $(".list-folder").load(url+" "+selector,function(response,status){
      setLoading(false);
      $('input[name="fileChecked"]').click(function () {  
    checkSelected();
  });
      checkSelected();
    });

  }

  function setLoading($tampil=false){
    if($tampil)
    {
      $(".loader").css("display", "block");
    }
    else{
      $(".loader").css("display", "none");
    }
    
  }
$(document).ready(function() {

  $('.dropdown-submenu a.test').on("click", function(e){
    $(this).next('ul').toggle();
    e.stopPropagation();
    e.preventDefault();
  });
  

  $('input[name="fileChecked"]').click(function () {  
    checkSelected();
  });
  checkSelected();

  $(".dropdown-toggle").click(function(event) {
    event.preventDefault();

  });
  

  
  
  $("#sortField").on("change",function (e) {
    e.preventDefault();
    var val = $(this).val();
    refreshContent(val);

    return false;
  });

  $("#sortAsc").on("click",function (e) {
    e.preventDefault();
    var field = $("#sortField").val();
    var sort = "true";
    var type = $(this).data("type");
    var html = "";
    if(type == "desc")
    {
      $(this).data('type','asc');
      $(this).attr('title','Sort Ascending');
      var sort = "true";
      var html = "<i class='fa fa-sort-amount-asc'></i>";
    }
    else if(type == "asc")
    {
      $(this).data('type','desc');
      $(this).attr('title','Sort Descending');
      var sort = "false";
      var html = "<i class='fa fa-sort-amount-desc'></i>";
    }
    $("#sortAsc").html(html);
    refreshContent(field,sort);
    return true;
  });

  function triggerFormModal(url,judul,id, selector=".form-ajax") {
    setLoading(true);

    $(".modal-title").html(judul);
    $(".modal-context").load(url+" "+selector,function(response,status){
      setLoading(false);
      
      var node = $(".node").val(id);
      var destination = $(".destination").val(id);
      $("form#form-input").submit(function() {
        if ($(this).valid()) {

          url = $(this).attr('action');

          var data = new FormData($(this)[0]);
          el = $(this);
          swal({
            title: 'Apakah Anda sudah yakin?',
            text: "Data akan disimpan",
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, simpan!'
          }).then(function () {
            setLoading(true);
            $.ajax({
              url: url,
              data: data,
             
              dataType: "JSON",
              cache: false,
              contentType: false,
              processData: false,
              async: false,
              type: "POST",
              success: function(data) {
                $(".loader").css("display", "none");
                if (data.status) {
                  swal(
                    'Berhasil!',
                    data.msg,
                    'success'
                    );
                  refreshContent(false);
                } else {
                  swal(
                    'Gagal',
                    data.msg,
                    'error'
                    )
                }
              },
              error: function(xhr, status) {
               setLoading(false);
               console.log(xhr);
               swal(
                'Gagal',
                'Data Gagal Disimpan',
                'error'
                )
             }
           });
          }).catch(swal.noop);
        }
        return false;
      });
    });
    $("#modal-trigger").trigger("click");

  }
  $("#page-wrapper").on("click",".act-modal",function () {
    var set = $(this).attr("id");
    var base_url = $(this).attr("href");
     var judul = $(this).data("title");
    var id = $(this).data("id");
    if(set == "create_folder"){
      var url = base_url+"document/operation/create_folder";
    }
    else if(set == "upload_file"){
      var url = base_url+"document/operation/upload_file";
    }else if(set == "edit_properties"){
      var url = base_url+"document/operation/edit_properties?nodeRef="+id;
    }
    
   
    triggerFormModal(url,judul,id);
    return false;
  });
  $(".create_template").on("click",function () {
    var node = $(this).data("node");
    var base_url = $(this).attr("href");
    var id = $(this).data("id");
    var title = $(this).data("title");
    var url = base_url+"document/operation/copy_file";
    $.ajax({
      url: url,             
      type: "POST",
      dataType: "JSON",
      data: 'title='+title+'&node='+node+'&id='+id,
      success: function(data) {
       setLoading(false);
       if (data.status) {
        swal(
          'Berhasil!',
          data.msg,
          'success'
          );
        refreshContent();
      } else {
        swal(
          'Gagal',
          data.msg,
          'error'
          )
      }
    },
    error: function(xhr, status) {
     setLoading(false);
     console.log(xhr);
     swal(
      'Gagal',
      'Data Gagal Disimpan',
      'error'
      )
   }
 });
    return false;
  });
  $(".select_files").on("click",function (e) {
    e.preventDefault();
    var type = $(this).data("type");
    if(type == "all")
    {
      $('input[name="fileChecked"]:checkbox').prop('checked', true);
    }
    else if(type == "documents")
    {
      $('input[name="fileChecked"]:checkbox').prop('checked', false);
      $('input[name="fileChecked"][class="file"]:checkbox').prop('checked', true);
    }
    else if(type == "folders")
    {
      $('input[name="fileChecked"]:checkbox').prop('checked', false);
      $('input[name="fileChecked"][class="folder"]:checkbox').prop('checked', true);
    }
    else if(type == "none")
    {
      $('input[name="fileChecked"]:checkbox').prop('checked', false);
    }
    else if(type == "invert")
    {
      if(!$('.file').prop('checked') && !$('.folder').prop('checked'))
      {
        $('input[name="fileChecked"]:checkbox').prop('checked', true);
      }
      else if($('.file').prop('checked') && $('.folder').prop('checked'))
      {
        $('input[name="fileChecked"]:checkbox').prop('checked', false);
      }
      if($('.file').prop('checked'))
      {
       $('input[name="fileChecked"][class="folder"]:checkbox').prop('checked', true);
       $('input[name="fileChecked"][class="file"]:checkbox').prop('checked', false);
     }
     else{
      $('input[name="fileChecked"][class="folder"]:checkbox').prop('checked', false);
      $('input[name="fileChecked"][class="file"]:checkbox').prop('checked', true);
    }
  }

  return true;
});
  $(".act-delete").on("click",function () {
    var url2 = $(this).attr("href");
    var url = $(".href").attr("href");
    swal({
      title: 'Apakah Anda sudah yakin?',
      text: "Data akan dihapus",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, hapus data!',
    }).then(function () {
      setLoading(true);
      $.ajax({
        url: url2,
        type: "POST",
        dataType: "JSON",
        data:{},
        success:function(res){
          if(res.status){

            swal(
              'Berhasil!',
              res.msg,
              'success'
              );


          } else {
            swal(
              'Gagal',
              res.msg,
              'error'
              )
          }
          setLoading(false);
        }, error: function(xhr,status){
          setLoading(false);
          swal(
            'Gagal',
            'Maaf, system error.',
            'error'
            )
        }
      });
    }).catch(swal.noop);;
    return false;

  });
  $("form#form-input").submit(function() {
    if ($(this).valid()) {
     var url2 = $(".href").attr("href");
     url = $(this).attr('action');

     var data = new FormData($(this)[0]);
     el = $(this);
     swal({
      title: 'Apakah Anda sudah yakin?',
      text: "Data akan disimpan",
      type: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, simpan!'
    }).then(function () {
      setLoading(true);
      $.ajax({
        url: url,
        data: data,
        dataType: "JSON",
        cache: false,
        contentType: false,
        processData: false,
        async: false,
        type: "POST",
        success: function(data) {
          $(".loader").css("display", "none");
          if (data.status) {
            swal(
              'Berhasil!',
              data.msg,
              'success'
              );
            refreshContent();
          } else {
            swal(
              'Gagal',
              data.msg,
              'error'
              )
          }
        },
        error: function(xhr, status) {
          setLoading(false);
          console.log(xhr);
          swal(
            'Gagal',
            'Data Gagal Disimpan',
            'error'
            )
        }
      });
    }).catch(swal.noop);
  }
  return false;
});


});




