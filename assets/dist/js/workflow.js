function startWorkflow(view,selector=".form-ajax"){
	var base_url = $('base').attr('href');
	var url = base_url+"document/workflow/start?view="+view;
	$("#workflow").load(url+" "+selector,function(response,status){
	});
}
$(document).ready(function(){
	$("#select-workflow").on("change",function(){
		var view = $(this).find(":selected").data('view');
		startWorkflow(view);
	});
});