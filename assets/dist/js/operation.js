function listFolder(){
 $('#list-folder-item').on('click','.list',function(e){
  e.preventDefault();
  $('.list-item a.active-path').removeClass('active-path');
  $(this).addClass('active-path');
  var path = $(this).data('path');
  var id = $(this).data('id');
  var base_url = $('base').attr('href');
  $.ajax({
    url: base_url+"context/mine/test2",
    data: 'path='+path,
    dataType: "html",         
    type: "POST",
    success: function(data) {
      console.log(id);
      var x = $('.list[data-id="'+id+'"]').clone().wrap('<a>').parent().html();
      $('#'+id).html(x+data);
      $('.list').on('click',function(e){
        e.preventDefault();
      });
    },
    error: function(xhr, status) {
      console.log(xhr);
    }
  });
});
}

function listDestination(){
 $('#destination-repository').on('click',function(e){
  e.preventDefault();
  var path = "";
  var base_url = $('base').attr('href');
  $.ajax({
    url: base_url+"context/mine/test2",
    data: 'path='+path,
    dataType: "html",         
    type: "POST",
    success: function(data) {
      $('#list-folder').html(data);
      $('.list').on('click',function(e){
        e.preventDefault();
      });
    },
    error: function(xhr, status) {
      console.log(xhr);
    }
  });
});
 $('#destination-shared').on('click',function(e){
  e.preventDefault();
  var path = "/Shared";
  var base_url = $('base').attr('href');
  $.ajax({
    url: base_url+"context/mine/test2",
    data: 'path='+path,
    dataType: "html",         
    type: "POST",
    success: function(data) {
      $('#list-folder').html(data);
      $('.list').on('click',function(e){
        e.preventDefault();
      });
    },
    error: function(xhr, status) {
      console.log(xhr);
    }
  });
});
 $('#destination-myfiles').on('click',function(e){
  e.preventDefault();
  var homeFolder = $('#homeFolder').data('name');
  var path = "/User%20Homes/"+homeFolder;
  var base_url = $('base').attr('href');
  $.ajax({
    url: base_url+"context/mine/test2",
    data: 'path='+path,
    dataType: "html",         
    type: "POST",
    success: function(data) {
      console.log(homeFolder);
      $('#list-folder').html(data);
      $('.list').on('click',function(e){
        e.preventDefault();
      });
    },
    error: function(xhr, status) {
      console.log(xhr);
    }
  });
});
}
$(document).ready(function() {


  function triggerFormDelete(url,judul,folder,file,folder_name,file_name,detail=false, selector=".form-ajax") {
    setLoading(true);

    $(".modal-title").html(judul);
    $(".modal-context").load(url+" "+selector,function(response,status){
      listFolder();
      listDestination();
      setLoading(false);
      if(!detail){
      for (var i = 0; i < folder_name.length; i++) {
       $('.nodeRefs').append("<a href='#'' class='list-group-item'><span class='glyphicon glyphicon-folder-open' aria-hidden='true'></span>&nbsp;&nbsp;"+folder_name[i]+"</a>");
     }
     for (var i = 0; i < file_name.length; i++) {
       $('.nodeRefs').append("<a href='#'' class='list-group-item'><span class='fa fa-file' aria-hidden='true'></span>&nbsp;&nbsp;"+file_name[i]+"</a>");
     }
   }
     
     $("form#form-input").submit(function() {
      if ($(this).valid()) {


        var path = $(".list-item a.active-path").data('node');
        var data = new FormData($(this)[0]);
        el = $(this);
        swal({
          title: 'Apakah Anda sudah yakin?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya'
        }).then(function () {
          setLoading(true);
          $.ajax({
           url: url,             
           type: "POST",
           dataType: "JSON",
           data: 'folder='+folder+'&file='+file+'&path='+path,
           success: function(data) {
            setLoading(false);
            if (data.status) {
              swal(
                'Berhasil!',
                data.msg,
                'success'
                );
              refreshContent();
            } else {
              swal(
                'Gagal',
                data.msg,
                'error'
                )
            }
          },
          error: function(xhr, status) {
           setLoading(false);
           console.log(xhr);
           swal(
            'Gagal',
            'Data Gagal Dihapus',
            'error'
            )
         }
       });
        }).catch(swal.noop);
      }
      return false;
    });
   });
    $("#modal-trigger").trigger("click");

  }
  $(".copy-to").on("click",function (e) {
    e.preventDefault();
    var base_url = $(this).attr("href");
    var judul = $(this).data("name");
    var type=$(this).data("type");
    if(type=="copy"){
      var url = base_url+"document/operation/copy_to";
    }
    else if(type=="move"){
      var url = base_url+"document/operation/move_to";
    }
    
    folder = $('input[name="fileChecked"][class="folder"]:checked').map(function(){
      return $(this).val();
    }).get();
    var file = $('input[name="fileChecked"][class="file"]:checked').map(function(){
      return $(this).val();
    }).get();
    folder_name = $('input[name="fileChecked"][class="folder"]:checked').map(function(){
      return $(this).data('name');
    }).get();
    var file_name = $('input[name="fileChecked"][class="file"]:checked').map(function(){
      return $(this).data('name');
    }).get();


    triggerFormDelete(url,judul,folder,file,folder_name,file_name);
    return false;
  });

  $("#page-wrapper").on("click",".copy-to-detail",function (e) {
    e.preventDefault();
    var base_url = $(this).attr("href");
    var judul = $(this).data("name");
    var type=$(this).data("type");
    var content = $(this).data("content");
    if(type=="copy"){
      var url = base_url+"document/operation/copy_to";
    }
    else if(type=="move"){
      var url = base_url+"document/operation/move_to";
    }
    var folder = null;
    var folder_name = null;
    var file = null;
    var file_name = null;
    if(content == "folder"){
      var folder = $(this).data("node");
      var folder_name = $(this).data("title");
    }
    else if(content == "document"){
      var file = $(this).data("node");
      var file_name = $(this).data("title");
    }

    triggerFormDelete(url,judul,folder,file,folder_name,file_name,true);
    return false;
  });

});




